class CreatePoems < ActiveRecord::Migration[5.1]
  def change
    create_table :poems do |t|
      t.references :user, foreign_key: true
      t.string :first
      t.string :second
      t.string :third
      t.integer :parent_id
      t.boolean :finish ,default: false

      t.timestamps
    end
  end
end
