class Poem < ApplicationRecord
  belongs_to :user
  
  validates :user_id, presence: true
  validates :first, presence: true, length: {minimum: 3, maximum: 8}
  validates :second, presence: true, length: {minimum: 3, maximum: 8}
  validates :third, length: {maximum: 6}
end
