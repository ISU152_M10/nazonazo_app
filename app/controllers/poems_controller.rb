class PoemsController < ApplicationController
  before_action :log_in, only: [:new,:create,:edit,:upate,:destroy]
  before_action :destroyable, only: [:destroy]
  before_action :editable, only: [:edit, :update] 
  
  
  
  def index
    @poems = Poem.where(parent_id: nil).paginate(page: params[:page],per_page:3)
    .order(id: "desc")
  end
  
  def show
    @poem = Poem.find_by(id: params[:id])
    if @poem.parent_id
      @poem =  Poem.find_by(id: @poem.parent_id)
    end
    @subsidiary_poems = Poem.where(parent_id: @poem.id)
  end
  
  def new
    if params[:parent_id]
      @parent_poem = Poem.find_by(id: params[:parent_id])
      if @parent_poem.finish
        flash[:notice] = "この作品はすでに完成にしてます"
        redirect_to @parent_poem
      end
      @poem= current_user.poems.build(parent_id: params[:parent_id])
    else
      @poem= current_user.poems.build(parent_id: nil)
    end
  end
  
  def create
    @parent_poem = Poem.find_by(id: params[:parent_id])
    if !@parent_poem.nil?
      if @parent_poem.finish
        flash[:notice] = "この作品はすでに完成にしてます"
        redirect_to @parent_poem
      end
    end
    
    @poem= current_user.poems.build(poem_params)
    if @poem.save
      if @poem.finish
        @parent_poem = Poem.find_by(id: @poem.parent_id)
        if @parent_poem
          @parent_poem.finish = true
          if @parent_poem.save
          end
        end
      end
      flash[:success] = '作成成功'
      redirect_to @poem
    else
      flash.now[:danger] = '入力内容が間違っています。'
      render 'new'
    end
    
  end
  
  def edit
    @poem = Poem.find_by(id: params[:id])
  end
  
  def update
    @poem = Poem.find_by(id: params[:id])
    if @poem.update_attributes(poem_params)
      flash[:success] = "編集成功"
      redirect_to @poem
    else
      flash[:danger] = "入力内容に間違いがあります"
      render 'edit'
    end
  end
  
  def destroy
    @poem = Poem.find_by(id: params[:id])
    parent_id = @poem.parent_id
    Poem.find_by(id: params[:id]).destroy
    flash[:success] = '削除成功'
    if !parent_id.nil?
      redirect_to Poem.find_by(id: parent_id)
    else
      redirect_to poems_url
    end
  end
  
  private
    def poem_params
      params.require(:poem).permit(:first, :second, :third,:parent_id, :finish)
    end
    
    def log_in
      if current_user.nil?
        flash[:notice] = "ログインしてください"
        redirect_to login_path
      end
    end
    
    def destroyable
      @poem = Poem.find_by(id: params[:id])
      unless allow_destroy(@poem)
        flash[:notice] = "権限がありません"
        redirect_to poems_url
      end
    end
    
    def editable
      @poem = Poem.find_by(id: params[:id])
      unless allow_edit(@poem)
        flash[:notice] = "権限がありません"
        redirect_to poems_url
      end
    end
    
end
