class SessionsController < ApplicationController
  
  def new
  end
  
  def create
    user = User.find_by(email: params[:sessions][:email])
    if user && user.authenticate(params[:sessions][:password])
      log_in user
      flash[:success] = "ログインしました。"
      redirect_to user
    else
      flash.now[:danger] = "入力情報が間違っています。"
      render 'new'
    end
  end
  
  def destroy
    logout
    redirect_to poems_url
  end
end
