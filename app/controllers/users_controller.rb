class UsersController < ApplicationController
  before_action :logged_in?, only: [:index, :edit, :update,:destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user?,     only: :destroy
  
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def show
    @user = User.find_by(id: params[:id])
    @user_poems = @user.poems.where(parent_id: nil).paginate(page: params[:page],per_page:2)
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "登録完了"
      log_in(@user)
      redirect_to @user
    else
      flash[:danger] = "入力項目が間違っています"
      render 'new'
    end
  end
  
  def edit
    @user = User.find_by(id: params[:id])
  end
  
  def update
    @user = User.find_by(id: params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "編集完了"
      redirect_to @user
    else
      flash[:danger] = "入力項目が間違っています"
      render 'edit'
    end
  end
  
  def destroy
    User.find_by(id: params[:id]).destroy
    flash[:notice] = 'ユーザーを削除しました。'
    redirect_to users_path
  end
  
  private
    def user_params
      params.require(:user).permit(:name,:email, :password,:password_confirmation)
    end
    
    def logged_in?
      unless log_in?
        flash[:danger] = "ログインしてください"
        redirect_to login_url
      end
    end
    
    def correct_user
      user = User.find_by(id: params[:id])
      unless current_user.id == user.id
        flash[:danger] = "編集権限がありません"
        redirect_to user_path(current_user)
      end
    end
  
    
    def admin_user?
      redirect_to root_url unless current_user.admin?
    end
end
