module PoemsHelper
  
  def finish_poem(poem)
    parent_poem = Poem.find_by(id: poem.parent_id)
    if parent_poem.nil?
      poem.finish
    else
      parent_poem.finish
    end
      
      
  end
  
  def allow_destroy(poem)
    if current_user.admin || 
      (current_user.id == poem.user.id && !finish_poem(poem))
      true
    else
      false
    end
  end
  
  def allow_edit(poem)
    if !finish_poem(poem) && current_user.id == poem.user.id
      true
    else
      false
    end  
  end
end
