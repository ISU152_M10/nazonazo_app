module SessionsHelper
  
  def log_in(user)
    session[:user_id] = user.id
  end
  
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end
  
  def log_in?
    !current_user.nil?
  end
  
  def current_user?(user)
    current_user.id == user.id
  end
 
  def logout
    session[:user_id] = nil
    @current_user = nil
  end
end
