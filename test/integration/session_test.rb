require 'test_helper'

class SessionTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:Harold)
  end
  
  test "login with right infomarion after logout" do
    get login_path
    post login_path, params: {sessions: {email: @user.email, 
                                        password: "password"}}
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path
    
    delete logout_path
    assert_redirected_to poems_url
    follow_redirect!
    assert_template 'poems/index'
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
  end
 
end
