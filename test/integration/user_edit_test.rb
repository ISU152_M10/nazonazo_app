require 'test_helper'

class UserEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:Harold)
    @other_user = users(:Doolittle)
  end
  
  test "failure with wrong user" do
    log_in(@user)
    get user_path(@other_user)
    assert_template 'users/show'
    get edit_user_path(@other_user)
    follow_redirect!
    assert_select 'div.alert-danger'
    assert_template 'users/show'
  end
  
  test "success edit" do
    log_in(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name = 'Gregory Boyington'
    email = 'pappy@us.marines'
    patch user_path(@user), params:{ user: { name: name,
                                      email: email, 
                                      password: "",
                                      password_confirmation: ""}}
    assert_redirected_to @user
    follow_redirect!
    assert_select 'div.alert-success'
    assert_template 'users/show'
  end
  
  test "failure edit with wrong info" do
    log_in(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params:{ user: { name: "",
                                      email: "", 
                                      password: "",
                                      password_confirmation: ""}}
    assert_template 'users/edit'
    assert_select 'div.alert-danger'
  end
  
  test 'require login for edit' do
    get edit_user_path(@user)
    assert_redirected_to login_url
    follow_redirect!
    assert_select 'div.alert-danger'
    assert_template 'sessions/new'
  end
  
   test 'require login for update' do
    patch user_path(@user), params:{ user: { name: "",
                                      email: "", 
                                      password: "",
                                      password_confirmation: ""}}
    assert_redirected_to login_url
    follow_redirect!
    assert_template 'sessions/new'
  end
end
