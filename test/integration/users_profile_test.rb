require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:Harold)
  end
  
  test "profile display" do
    log_in(@user)
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', text: "名前:"+@user.name

    assert_match @user.poems.count.to_s, response.body
    #assert_select 'div.pagination' テスト不可
    @user.poems.where(parent_id: nil).paginate(page: 1).each do |poem|
      assert_match poem.first, response.body
    end
  end
end
