require 'test_helper'

class StaticPageControllerTest < ActionDispatch::IntegrationTest
  test "should get abouts" do
    get about_path
    assert_template 'static_page/about'
  end
end
