ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include ApplicationHelper

  # Add more helper methods to be used by all tests here...
  def log_in(user)
    get login_path
    post login_path, params: {sessions: {email: user.email,
                                     password: 'password'}}
    assert_redirected_to user
    follow_redirect!
    assert_select 'div.alert-success'
    assert_template 'users/show'
  end
  
  def logged_in?
    !session[:user_id].nil?
  end
  
  def question
    get create_path
    assert_response :success
  end
end
