require 'test_helper'

class PoemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @user = users(:Harold)
    @poem = @user.poems.build(
                     first: "歌よみは",
                     second:"下手こそよけれ",
                     third:"",
                     parent_id: nil,
                     finish: false)
  end
  
  test "should be valid" do
    assert @poem.valid?
  end

  test "user id should be present" do
    @poem.user_id = nil
    assert_not @poem.valid?
  end

  test "first should not be too short" do
    @poem.first = "a"*2
    assert_not @poem.valid?
  end
  
  test "first should not be too long" do
    @poem.first = "a"*9
    assert_not @poem.valid?
  end
  
  test "second should not be too short" do
    @poem.second = "a"*2
    assert_not @poem.valid?
  end
  
  test "second should not be too long" do
    @poem.second = "a"*9
    assert_not @poem.valid?
  end
  
  test "third should be at most 6 characters" do
    @poem.third = "a"*7
    assert_not @poem.valid?
  end
end
