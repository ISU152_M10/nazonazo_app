Rails.application.routes.draw do
  get '/poems' ,to: 'poems#index'

  get '/login',to: 'sessions#new'
  post '/login',to: 'sessions#create'
  delete '/logout',to: 'sessions#destroy'

  get '/signup' ,to:'users#new'
  get '/index', to: 'users#index'

  root 'poems#index'
  get '/about' ,to: "static_page#about"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  resources :poems
end
